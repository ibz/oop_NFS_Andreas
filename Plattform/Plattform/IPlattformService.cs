﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Plattform.DB;
using Plattform.Models;
using Plattform.AirlineService;

namespace Plattform
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change
    // the interface name "IPlattformService" in both code and config file
    // together.
    [ServiceContract]
    public interface IPlattformService
    {
        [OperationContract]
        List<SpecialOffer> GetSpecialOffers();
        [OperationContract]
        bool ReserveSpecialOffer(SpecialOffer offer);
        [OperationContract]
        bool BookSpecialOffer(SpecialOffer offer);
        [OperationContract]
        bool CancelSpecialOffer(SpecialOffer offer);
        [OperationContract]
        List<Gender> GetGenders();
        [OperationContract]
        List<Salutation> GetSalutations();
        [OperationContract]
        List<City> GetCities();
    }
}
