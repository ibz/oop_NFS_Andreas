﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Plattform.DB;
using Plattform.Models;
using Plattform.AirlineService;


namespace Plattform
{
    public partial class FormHotel : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LabelName.ForeColor = System.Drawing.Color.Red;
            LabelZipCode.ForeColor = System.Drawing.Color.Red;
            LabelCityName.ForeColor = System.Drawing.Color.Red;
        }

        protected void TextBoxHotelName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBoxZipCode_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBoxCityName_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ButtonAddHotel_Click(object sender, EventArgs e)
        {
            LabelName.Text = "";
            LabelZipCode.Text = "";
            LabelCityName.Text = "";

            if (TextBoxHotelName.Text == "")
            {
               LabelName.Text = "Please enter a hotel name.";
            }

            if (TextBoxZipCode.Text == "")
            {
               LabelZipCode.Text = "Please enter a ZIP code.";
            }

            if (TextBoxCityName.Text == "")
            {
               LabelCityName.Text = "Please enter a city name";
            }

            if (LabelName.Text == "" && LabelZipCode.Text == ""
                && LabelCityName.Text == "")
            {
                HotelDB hotelDB = new HotelDB();
                Hotel hotel = new Hotel
                {
                    Name = TextBoxHotelName.Text,
                    City = new City
                    {
                        ZipCode = int.Parse(TextBoxZipCode.Text),
                        Name = TextBoxCityName.Text
                    }
                };
                hotelDB.CreateHotel(hotel);
                TextBoxHotelName.Text = string.Empty;
                TextBoxZipCode.Text = string.Empty;
                TextBoxCityName.Text = string.Empty;
            }
        }
    }
}
