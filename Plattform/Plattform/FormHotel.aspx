﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormHotel.aspx.cs" Inherits="Plattform.FormHotel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p><a href="Default.aspx">Home</a></p>
        <h1>Add a new hotel</h1>
        <h5>Name</h5>
        <asp:Label ID="LabelName" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxHotelName" runat="server" OnTextChanged="TextBoxHotelName_TextChanged"></asp:TextBox>
        <h5>Zip Code</h5>
        <asp:Label ID="LabelZipCode" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxZipCode" type="number" runat="server" OnTextChanged="TextBoxZipCode_TextChanged"></asp:TextBox>
        <h5>City</h5>
        <asp:Label ID="LabelCityName" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxCityName" runat="server" OnTextChanged="TextBoxCityName_TextChanged"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="ButtonAddHotel" runat="server" Text="Add Hotel" OnClick="ButtonAddHotel_Click" />
    </form>
</body>
</html>
