﻿using System.Runtime.Serialization;

namespace Plattform.Models
{
    [DataContract]
    public class RoomType
    {
        [DataMember]
        public int RoomTypeID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Capacity { get; set; }
        [DataMember]
        public string Inventory { get; set; }

        public RoomType() { }
        public RoomType(string name, int capacity, string inventory)
        {
            this.Name = name;
            this.Capacity = capacity;
            this.Inventory = inventory;
        }
    }
}
