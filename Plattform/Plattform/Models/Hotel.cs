﻿using System.Runtime.Serialization;
using Plattform.AirlineService;

namespace Plattform.Models
{
    [DataContract]
    public class Hotel
    {
        [DataMember]
        public int HotelID { get; set; }
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public virtual City City { get; set; }
    }
}
