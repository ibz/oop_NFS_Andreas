﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Plattform.Models
{
    [DataContract]
    public class Room
    {
        [DataMember]
        public int RoomID { get; set; }
        [DataMember]
        public RoomType RoomType { get; set; }
        [DataMember]
        public Hotel Hotel { get; set; }
        [DataMember]
        public DateTime FreeFrom { get; set; }
        [DataMember]
        public DateTime FreeUntil { get; set; }

        public Room() { }
        public Room(RoomType roomType, Hotel hotel, DateTime freeFrom,
                    DateTime freeUntil)
        {
           this.RoomType = roomType;
           this.Hotel = hotel;
           this.FreeFrom = freeFrom;
           this.FreeUntil = freeUntil;
        }
    }
}
