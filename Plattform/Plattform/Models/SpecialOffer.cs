﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Plattform.AirlineService;

namespace Plattform.Models
{
    [DataContract]
    public class SpecialOffer
    {
        [DataMember]
        public int SpecialOfferID { get; set; }
        [DataMember]
        public virtual Flight FlightTo { get; set; }
        [DataMember]
        public virtual Flight FlightFrom { get; set; }
        [DataMember]
        public virtual Person Customer { get; set; }
        [DataMember]
        public virtual Room Room { get; set; }
        [DataMember]
        public float Price { get; set; }
        [DataMember]
        public bool Reserverd { get; set; }
        [DataMember]
        public DateTime ReservationDate { get; set; }
        [DataMember]
        public bool Booked { get; set; }

        public SpecialOffer() { }
        public SpecialOffer(Flight flightTo, Flight flightFrom, Room room, float price)
        {
           this.FlightTo = flightTo;
           this.FlightFrom = flightFrom;
           this.Room = room;
           this.Price = price;
           this.Reserverd = false;
           this.Booked = false;
        }
    }
}
