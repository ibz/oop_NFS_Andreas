﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Plattform.DB;
using Plattform.Models;
using Plattform.AirlineService;

namespace Plattform
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change
    // the class name "PlattformService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please
    // select PlattformService.svc or PlattformService.svc.cs at the Solution
    // Explorer and start debugging.
    public class PlattformService : IPlattformService
    {
        SpecialOfferDB offerDB = new SpecialOfferDB();
        GenderDB genderDB = new GenderDB();
        SalutationDB salutationDB = new SalutationDB();
        AirlineServiceClient client = new AirlineServiceClient();
        public List<SpecialOffer> GetSpecialOffers()
        {
            try
            {
               return offerDB.GetAllSpecialOffers();
            }
            catch(Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return new List<SpecialOffer>();
            }
        }
        public bool ReserveSpecialOffer(SpecialOffer offer)
        {
           try
           {
              client.BookFlight(offer.FlightTo, offer.Room.RoomType.Capacity);
              client.BookFlight(offer.FlightFrom, offer.Room.RoomType.Capacity);
              offer.Reserverd = true;
              offerDB.UpdateSpecialOffer(offer);
              return true;
           }
           catch(Exception e)
           {
              System.Diagnostics.Trace.WriteLine(e);
              return false;
           }
        }
        public bool BookSpecialOffer(SpecialOffer offer)
        {
           try
           {
              offer.Booked = true;
              offerDB.UpdateSpecialOffer(offer);
              return true;
           }
           catch(Exception e)
           {
              System.Diagnostics.Trace.WriteLine(e);
              return false;
           }
        }
        public bool CancelSpecialOffer(SpecialOffer offer)
        {
           try
           {
              client.CancelFlight(offer.FlightTo, offer.Room.RoomType.Capacity);
              client.CancelFlight(offer.FlightFrom, offer.Room.RoomType.Capacity);
              offer.Reserverd = false;
              offerDB.UpdateSpecialOffer(offer);
              return true;
           }
           catch(Exception e)
           {
              System.Diagnostics.Trace.WriteLine(e);
              return false;
           }
        }
        public List<Gender> GetGenders()
        {
            try
            {
               return genderDB.GetAllGenders();
            }
            catch(Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return new List<Gender>();
            }
        }
        public List<Salutation> GetSalutations()
        {
            try
            {
               return salutationDB.GetAllSalutations();
            }
            catch(Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return new List<Salutation>();
            }
        }
        public List<City> GetCities()
        {
            CityDB cityDB = new CityDB();
            try
            {
              return cityDB.GetAllCities();
            }
            catch(Exception e)
            {
               System.Diagnostics.Trace.WriteLine(e);
               return new List<City>();
            }
        }
    }
}
