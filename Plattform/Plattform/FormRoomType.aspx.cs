﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Plattform.DB;
using Plattform.Models;

namespace Plattform
{
    public partial class FormRoomType: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            LabelName.ForeColor = System.Drawing.Color.Red;
            LabelCapacity.ForeColor = System.Drawing.Color.Red;
            LabelInventory.ForeColor = System.Drawing.Color.Red;
        }

        protected void TextBoxRoomType_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBoxCapacity_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBoxInventory_TextChanged(object sender, EventArgs e)
        {

        }

        protected void ButtonAddRoomType_Click(object sender, EventArgs e)
        {
            LabelName.Text = "";
            LabelCapacity.Text = "";
            LabelInventory.Text = "";

            if (TextBoxRoomType.Text == "")
            {
               LabelName.Text = "Please enter a name.";
            }
            if (TextBoxCapacity.Text == "")
            {
               LabelCapacity.Text = "Please enter capacity.";
            }
            if (TextBoxInventory.Text == "")
            {
               LabelInventory.Text = "Please describe the inventory.";
            }

            if (LabelName.Text == "" && LabelCapacity.Text == ""
                && LabelInventory.Text == "")
            {
                RoomTypeDB roomTypeDB = new RoomTypeDB();
                RoomType roomType = new RoomType
                {
                    Name = TextBoxRoomType.Text,
                    Capacity = int.Parse(TextBoxCapacity.Text),
                    Inventory = TextBoxInventory.Text
                };
                roomTypeDB.CreateRoomType(roomType);
                TextBoxRoomType.Text = string.Empty;
                TextBoxCapacity.Text = string.Empty;
                TextBoxInventory.Text = string.Empty;
            }
        }
    }
}
