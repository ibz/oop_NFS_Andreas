﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.Models;
using Plattform.AirlineService;

namespace Plattform.DB
{
    public class HotelDB
    {
        public List<Hotel> GetAllHotels()
        {
            using (Context ctx = new Context())
            {
                return ctx.Hotels.ToList();
            }
        }
        public bool CreateHotel(Hotel hotel)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Hotels.Add(hotel);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateHotel(Hotel hotel)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Hotels.Attach(hotel);
                    ctx.Entry(hotel).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteHotel(Hotel hotel)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Hotels.Attach(hotel);
                    ctx.Hotels.Remove(hotel);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
