﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.Models;

namespace Plattform.DB
{
    public class RoomDB
    {
        public List<Room> GetAllRooms()
        {
            using (Context ctx = new Context())
            {
                return ctx.Rooms.ToList();
            }
        }
        public bool CreateRoom(Room room)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Rooms.Add(room);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateRoom(Room room)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Rooms.Attach(room);
                    ctx.Entry(room).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteRoom(Room room)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Rooms.Attach(room);
                    ctx.Rooms.Remove(room);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
