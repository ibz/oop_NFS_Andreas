﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.Models;
using Plattform.AirlineService;

namespace Plattform.DB
{
    public class AvailabilityDB
    {
        public List<Availability> GetAllAvailabilities()
        {
            using (Context ctx = new Context())
            {
                return ctx.Availabilities.ToList();
            }
        }
        public bool CreateAvailability(Availability availability)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Availabilities.Add(availability);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateAvailability(Availability availability)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Availabilities.Attach(availability);
                    ctx.Entry(availability).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteAvailability(Availability availability)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Availabilities.Attach(availability);
                    ctx.Availabilities.Remove(availability);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
