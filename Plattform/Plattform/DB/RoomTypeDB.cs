﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.Models;
using System.Diagnostics;

namespace Plattform.DB
{
    public class RoomTypeDB
    {
        public List<RoomType> GetAllRoomTypes()
        {
            using (Context ctx = new Context())
            {
                return ctx.RoomTypes.ToList();
            }
        }
        public bool CreateRoomType(RoomType roomType)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.RoomTypes.Add(roomType);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateRoomType(RoomType roomType)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.RoomTypes.Attach(roomType);
                    ctx.Entry(roomType).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteRoomType(RoomType roomType)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.RoomTypes.Attach(roomType);
                    ctx.RoomTypes.Remove(roomType);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
