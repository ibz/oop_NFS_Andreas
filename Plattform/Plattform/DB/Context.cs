﻿using System.Data.Entity;
using Plattform.Models;
using Plattform.Helper;
using Plattform.AirlineService;

namespace Plattform.DB
{
    public class Context : DbContext
    {
        public Context() : base("PlattformDB") { }

        public DbSet<City> Cities { get; set; }
        public DbSet<Airline> Airlines { get; set; }
        public DbSet<Flight> Flights { get; set; }
        public DbSet<Hotel> Hotels { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }
        public DbSet<Availability> Availabilities { get; set; }
        public DbSet<Airport> Airports { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<Salutation> Salutations { get; set; }
        public DbSet<SpecialOffer> SpecialOffers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AirlineService.City>()
                .Property(c => c.Name)
                .HasMaxLength(50)
                .IsRequired();

            modelBuilder.Entity<AirlineService.City>()
                .Property(c => c.ZipCode)
                .IsRequired()
                .IsUnique();

            modelBuilder.Entity<AirlineService.Airline>()
                .Property(a => a.AirlineID)
                .IsRequired()
                .IsUnique();

            modelBuilder.Entity<AirlineService.Airline>()
                .Property(a => a.Name)
                .IsRequired();

            modelBuilder.Entity<AirlineService.Flight>()
                .Property(f => f.Name)
                .IsRequired();

            modelBuilder.Entity<AirlineService.Flight>()
                .Property(f => f.Duration)
                .IsRequired();

            modelBuilder.Entity<AirlineService.Flight>()
                .Property(f => f.StartTime)
                .IsRequired();

            modelBuilder.Entity<AirlineService.Airport>()
                .HasKey(f => f.ShortName);
        }
    }
}
