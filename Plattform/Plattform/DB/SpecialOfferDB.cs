﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.Models;

namespace Plattform.DB
{
    public class SpecialOfferDB
    {
        public List<SpecialOffer> GetAllSpecialOffers()
        {
            using (Context ctx = new Context())
            {
                return ctx.SpecialOffers.ToList();
            }
        }
        public bool CreateSpecialOffer(SpecialOffer specialOffer)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.SpecialOffers.Add(specialOffer);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateSpecialOffer(SpecialOffer specialOffer)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.SpecialOffers.Attach(specialOffer);
                    ctx.Entry(specialOffer).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteSpecialOffer(SpecialOffer specialOffer)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.SpecialOffers.Attach(specialOffer);
                    ctx.SpecialOffers.Remove(specialOffer);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
