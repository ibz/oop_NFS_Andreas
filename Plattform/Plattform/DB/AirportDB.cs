﻿using System;
using System.Collections.Generic;
using System.Linq;
using Plattform.AirlineService;

namespace Plattform.DB
{
    public class AirportDB
    {
        public List<Airport> GetAllAirports()
        {
            using (Context ctx = new Context())
            {
                return ctx.Airports.ToList();
            }
        }
        public bool CreateAirport(Airport airport)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Airports.Add(airport);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }

        public bool UpdateAirport(Airport airport)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Airports.Attach(airport);
                    ctx.Entry(airport).State = System.Data.Entity.EntityState.Modified;
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }

        }
        public bool DeleteAirport(Airport airport)
        {
            try
            {
                using (Context ctx = new Context())
                {
                    ctx.Airports.Attach(airport);
                    ctx.Airports.Remove(airport);
                    ctx.SaveChanges();
                }
                return true;
            }
            catch (Exception e)
            {
                System.Diagnostics.Trace.WriteLine(e);
                return false;
            }
        }
    }
}
