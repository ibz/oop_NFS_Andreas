﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Plattform.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h5>RoomType
        </h5>
        <asp:Label ID="LabelRoomType" runat="server" Text=""></asp:Label>
        <br />
        <asp:DropDownList ID="DropDownRoomType" runat="server"
            OnSelectedIndexChanged="DropDownRoomType_SelectedIndexChanged"
            AppendDataBoundItems="true">
            <asp:ListItem Text="-- Choose one --" Value=""/>
        </asp:DropDownList>
        <br />
        <a href="FormRoomType.aspx">Add a room type</a>
        <br />
        <br />
        <h5>Hotel</h5>
        <asp:Label ID="LabelHotel" runat="server" Text=""></asp:Label>
        <br />
        <asp:DropDownList ID="DropDownHotel" runat="server"
            OnSelectedIndexChanged="DropDownHotel_SelectedIndexChanged"
            AppendDataBoundItems="true">
            <asp:ListItem Text="-- Choose one --" Value=""/>
        </asp:DropDownList>
        <br />
        <a href="FormHotel.aspx">Add a hotel</a>
        <br />
        <br />
        <h3>Availability
        </h3>
        <asp:Label ID="LabelAvailability" runat="server" Text=""></asp:Label>
        <br />
        <h5>From</h5>
        <asp:Calendar ID="CalendarFrom" runat="server" OnSelectionChanged="CalendarFrom_SelectionChanged"></asp:Calendar>
        <h5>To</h5>
        <asp:Calendar ID="CalendarTo" runat="server" OnSelectionChanged="CalendarTo_SelectionChanged"></asp:Calendar>
        <br />
        <asp:Button ID="ButtonAddRoom" runat="server" OnClick="ButtonAddRoom_Click" Text="Add Room" />
        <br />
    </form>
</body>
</html>
