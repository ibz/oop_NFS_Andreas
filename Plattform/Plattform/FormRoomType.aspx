﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="FormRoomType.aspx.cs" Inherits="Plattform.FormRoomType" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <p><a href="Default.aspx">Home</a></p>
        <h1>Add a new room type</h1>
        <h5>Name</h5>
        <asp:Label ID="LabelName" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxRoomType" runat="server" OnTextChanged="TextBoxRoomType_TextChanged"></asp:TextBox>
        <h5>Capacity</h5>
        <asp:Label ID="LabelCapacity" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxCapacity" type="number" runat="server" OnTextChanged="TextBoxCapacity_TextChanged"></asp:TextBox>
        <h5>Intentory</h5>
        <asp:Label ID="LabelInventory" runat="server" Text=""></asp:Label>
        <br />
        <asp:TextBox ID="TextBoxInventory" runat="server" OnTextChanged="TextBoxInventory_TextChanged"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="ButtonAddRoomType" runat="server" Text="Add Room Type" OnClick="ButtonAddRoomType_Click" />
    </form>
</body>
</html>
