﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Plattform.Models;
using Plattform.DB;
using Plattform.AirlineService;

namespace Plattform
{
    public partial class Default: System.Web.UI.Page
    {
        List<Hotel> Hotels { get; set; }
        List<RoomType> RoomTypes { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            HotelDB hotelDB = new HotelDB();
            RoomTypeDB roomTypeDB = new RoomTypeDB();

            this.Hotels = hotelDB.GetAllHotels();
            DropDownHotel.DataSource = Hotels;
            DropDownHotel.DataTextField = "Name";
            DropDownHotel.DataValueField = "HotelID";
            DropDownHotel.DataBind();

            RoomTypes = roomTypeDB.GetAllRoomTypes();
            DropDownRoomType.DataSource = RoomTypes;
            DropDownRoomType.DataTextField = "Name";
            DropDownRoomType.DataValueField = "RoomTypeID";
            DropDownRoomType.DataBind();

            LabelHotel.ForeColor = System.Drawing.Color.Red;
            LabelRoomType.ForeColor = System.Drawing.Color.Red;
            LabelAvailability.ForeColor = System.Drawing.Color.Red;
        }

        protected void DropDownRoomType_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownHotel_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void CalendarFrom_SelectionChanged(object sender, EventArgs e)
        {

        }

        protected void CalendarTo_SelectionChanged(object sender, EventArgs e)
        {

        }
        protected void ButtonAddRoom_Click(object sender, EventArgs e)
        {
            AirlineServiceClient client = new AirlineServiceClient();
            LabelHotel.Text = "";
            LabelRoomType.Text = "";
            LabelAvailability.Text = "";
            RoomDB roomDB = new RoomDB();
            SpecialOfferDB offerDB = new SpecialOfferDB();
            if ((CalendarFrom.SelectedDate.Date == DateTime.MinValue.Date
                | CalendarTo.SelectedDate.Date == DateTime.MinValue.Date))
            {
               LabelAvailability.Text = "Please select a From and Until date.";
            }
            if (DropDownHotel.SelectedValue == "")
            {
               LabelHotel.Text = "Please selet a hotel.";
            }
            if(DropDownRoomType.SelectedValue == "")
            {
               LabelRoomType.Text = "Please select a room type.";
            }
            if (LabelHotel.Text == "" && LabelRoomType.Text == ""
                && LabelAvailability.Text == "")
            {
                Room room = new Room
                {
                    Hotel = this.Hotels.Single(h => h.HotelID == int.Parse(DropDownHotel.SelectedValue)),
                    RoomType = this.RoomTypes.Single(t => t.RoomTypeID == int.Parse(DropDownRoomType.SelectedValue)),
                    FreeFrom = CalendarFrom.SelectedDate,
                    FreeUntil = CalendarTo.SelectedDate
                };
                Dictionary<string, List<Flight>> flights =
                   new Dictionary<string, List<Flight>>();
                List<SpecialOffer> offers = new List<SpecialOffer>();
                var flightsx = client.GetFlights(room.FreeFrom, room.FreeUntil,
                                            room.Hotel.City.Name,
                                            room.RoomType.Capacity);
                foreach (var flight in flightsx["To"])
                {
                   SpecialOffer offer = new SpecialOffer();
                   offer.FlightTo = flight;
                   offer.Room = room;
                   offer.Price = 100;
                   offers.Add(offer);
                }
                int counter = 0;
                foreach (var offer in offers)
                {
                   offer.FlightFrom = flightsx["From"][counter];
                   offerDB.CreateSpecialOffer(offer);
                   counter += 1;
                }
                // roomDB.CreateRoom(room);
                DropDownHotel.ClearSelection();
                DropDownRoomType.ClearSelection();
                CalendarFrom.SelectedDates.Clear();
                CalendarTo.SelectedDates.Clear();
            }
        }
    }
}
