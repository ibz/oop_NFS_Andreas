﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WPFClient.PlattformService;

namespace WPFClient
{
    /// <summary>
    /// Interaction logic for CustomerDetails.xaml
    /// </summary>
    public partial class CustomerDetails : Window
    {
        public Person Person { get; set; }
        public List<City> Cities { get; set; }
        public List<Gender> Genders { get; set;  }
        public List<Salutation> Salutations { get; set;  }

        public CustomerDetails()
        {
            InitializeComponent();
            PlattformServiceClient client = new PlattformServiceClient();

            // get the data from the service
            this.Genders = client.GetGenders();
            this.Salutations = client.GetSalutations();
            this.Cities = client.GetCities();

            // populate the dropdowns
            this.DropDownCity.ItemSource = this.Cities;
            this.DropDownCity.DisplayMemberPath = "Name";
            this.DropDownGender.ItemSource = this.Genders;
            this.DropDownGender.DisplayMemberPath = "Name";
            this.DropDownSalutation.ItemSource = this.Salutations;
            this.DropDownSalutation.DisplayMemberPath = "Name";
            client.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           Person person = new Person();
           person.Gender = (Gender)DropDownGender.SelectedValue;
           person.Salutation = (Salutation)DropDownSalutation.SelectedValue;
           person.City = (City)DropDownCity.SelectedValue;
           person.FirstName = TextBoxFirstName.Text;
           person.LastName = TextBoxLastName.Text;
           person.Streetname = TextBoxStreetname.Text;
           person.Streenumber = TextBoxStreetnumber.Text;
           this.person = person;
        }
    }
}
